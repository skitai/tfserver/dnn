"""
Hans Roh 2015 -- http://osp.skitai.com
License: BSD
"""
import re
import sys
import os
import shutil, glob
from warnings import warn
try:
	from setuptools import setup
except ImportError:
	from distutils.core import setup

with open('dnn/__init__.py', 'r') as fd:
	version = re.search(r'^__version__\s*=\s*"(.*?)"',fd.read(), re.M).group(1)

if 'publish' in sys.argv:
	os.system ('{} setup.py bdist_wheel'.format (sys.executable))
	whl = glob.glob ('dist/dnn-{}-*.whl'.format (version))[0]
	os.system ('twine upload {}'.format (whl))
	sys.exit ()

classifiers = [
  	'License :: OSI Approved :: MIT License',
  	'Development Status :: 1 - Planning',
	'Environment :: Console',
	'Topic :: Software Development :: Libraries :: Python Modules',
	'Topic :: Scientific/Engineering :: Artificial Intelligence',
	'Intended Audience :: Developers',
	'Intended Audience :: Science/Research',
	'Programming Language :: Python',
	'Programming Language :: Python :: 3'
]

packages = [
	'dnn',
	'dnn.costs',
	'dnn.losses',
	'dnn.callbacks',
    'dnn.metrics',
	'dnn.processing.video',
	'dnn.processing.audio',
	'dnn.processing.text',
	'dnn.processing.image',
	'dnn.processing.image.retinaface',
	'dnn.processing.store',
	'dnn.custlayers',
	'dnn.problems.od',
	'dnn.problems.segmentation',
	'dnn.problems.similarity',
]

package_dir = {'dnn': 'dnn'}

install_requires = [
	"rs4",
	"tfserver",
	"numpy",
	"scikit-learn",
	"future",
	"networkx",
    "hyperopt",
	"protobuf>=3.20",
	"dill",
    "webrtcvad",
	'exif',
	'fdet',
]

package_data = {
	"dnn": [
		'processing/image/retinaface/model/7/*',
		'processing/image/retinaface/model/7/assets/*',
		'processing/image/facenet/*.md',
		'processing/audio/noise_waves/*.md',
	]
}


if __name__ == "__main__":
	with open ('README.rst', encoding='utf-8') as f:
		long_description = f.read ()

	setup(
		name='dnn',
		version=version,
		description='Machine Learning Utilities',
		long_description = long_description,
		url = 'https://gitlab.com/tfserver/dnn',
		author='Hans Roh',
		author_email='hansroh@gmail.com',
		packages=packages,
		package_dir=package_dir,
		license='MIT',
		platforms = ["posix",],
		download_url = "https://pypi.python.org/pypi/dnn",
		install_requires = install_requires,
		classifiers=classifiers,
		package_data = package_data
	)
