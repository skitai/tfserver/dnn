from dnn import datautil
from dnn.label import Label
from datasets import dlna
import numpy as np
from tqdm import tqdm
import tensorflow as tf
import dnn
from dnn import predutil, costs
import sys
import shutil, os

LABEL = Label ([0, 1], "label")
N_VOCA = 4120
RNN_SIZE = 128
LEARNING_RATE = 0.001
DECAY_RATE = 0.94
BATCH_SIZE = 32
DROPOUT_RATE = 0.5
EPOCHS = 50
ISTEST = False

class DNN (dnn.DNN):
    def make_place_holders (self):
        self.n_class = len (self.labels [0])
        self.x = tf.compat.v1.placeholder ("float32", [None, dlna.MAX_TERMS])
        self.y = tf.compat.v1.placeholder ("float32", [None, self.n_class])
        self.use_seq_length ()

    def make_optimizer (self):
        return self.optimizer ("adam")

    def make_logit (self):
        with tf.compat.v1.variable_scope ("embedding"):
            embed = self.embeddings (self.x, N_VOCA, RNN_SIZE)
        with tf.compat.v1.variable_scope ("lstm"):
            outputs, state = self.lstm (embed, RNN_SIZE, dropout = True)
            layer = tf.reduce_max (input_tensor=outputs, axis=[1])
        with tf.compat.v1.variable_scope ("dnn"):
            layer = self.dense (layer, RNN_SIZE // 2, kreg = self.l2 (0.1))
            layer = self.batch_norm_with_dropout (layer, activation = self.swish)
            layer = self.dense (layer, RNN_SIZE // 4, kreg = self.l2 (0.1))
            layer = self.batch_norm_with_dropout (layer, activation = self.swish)
            layer = self.dense (layer, self.n_class, activation = tf.nn.softmax)
        return layer

    def make_cost (self):
        lossfunc = costs.categorical_crossentropy
        return tf.reduce_mean (input_tensor=lossfunc (logits = self.logit, labels = self.y))

    def performance_metric (self, r):
        return r.metric.f1.weighted [0]


def ftest (export = True, test = False):
    print ("* Restoring checkpoint...")
    net = DNN ()
    net.set_train_dir ("temp/checkpoints")
    try:
        net.restore ()
    except ValueError:
        print ("* Error: checkpoint is empty")
        return

    if export:
        version = net.to_save_model (
            "temp/models",
            'predict',
            inputs = {'x': net.x, "seq_length": net.seq_length},
            outputs={'y': net.logit}
        )
        print ("* Model exported: version {}".format (version))

    test_xs, test_ys = dlna.get_testset (test = test)
    test_ys = LABEL.onehots (test_ys)
    print ("\n* Final testing: {:,}".format (len (test_ys)))
    r = net.ftest (test_xs, test_ys, seq_length = dlna.eos (test_xs))

    print (test_xs [0].tolist (), test_ys [0].tolist (), dlna.eos (test_xs) [0])

    return 1.0 - r.get_performance ()

def train (test = False):
    print ("* Data loading...")
    train_xs, valid_xs, train_ys, valid_ys = dlna.get_trainset (10000, test = test)
    STEPS = len (train_ys) // BATCH_SIZE + 1
    print ("* Data stat")
    print ("  - Vocabulary: {:,}".format (N_VOCA))
    print ("  - Training data: {:,}".format (len (train_ys)))
    print ("  - Validating data: {:,}".format (len (valid_ys)))

    valid_ys = LABEL.onehots (valid_ys)
    train_ys = LABEL.onehots (train_ys)

    net = DNN ()
    net.set_labels (LABEL)
    net.set_train_dir ("./temp/checkpoints", True, True, improve_metric = "performance")
    net.set_tensorboard_dir ("./temp/tflog")
    net.set_learning_rate (LEARNING_RATE, DECAY_RATE, decay_step = STEPS)
    net.setup ()

    print ("* Kernels")
    for k, v in net.count_kernels ():
        print ("  - {}: {}".format (k, v))

    print("* Start training...")
    cherry = None
    resub_minibatches = datautil.serial_minibatch (train_xs, train_ys, len (valid_xs), infinite = True)
    for epoch in range (1, EPOCHS + 1):
        print ("\n* Epoch progress: {} / {} ({:.0f}%)".format (epoch, EPOCHS, epoch / EPOCHS * 100))
        train_minibatches = datautil.serial_minibatch (train_xs, train_ys, BATCH_SIZE)
        batchbar = datautil.get_tqdm (STEPS, "+ Step", "blue")
        for step, (batch_xs, batch_ys) in enumerate (train_minibatches):
            batchbar.update (1)
            net.set_epoch (epoch)
            net.batch (batch_xs, batch_ys, dropout_rate = DROPOUT_RATE, seq_length = dlna.eos (batch_xs))
        batchbar.close ()

        resub_xs, resub_ys = next (resub_minibatches)
        net.resub (resub_xs, resub_ys, seq_length = dlna.eos (resub_xs)).log ()

        r = net.valid (valid_xs, valid_ys, seq_length = dlna.eos (valid_xs)).log ()
        r.confusion_matrix ()

        net.cherry.log ()
        net.cherry.confusion_matrix ()

        if net.is_overfit:
            print ("* Overfit in progress, terminated.")
            break

    print ("* Training completed.")
    return 1.0 - net.cherry.get_performance ()


def main (test = False):
    global ISTEST, EPOCHS, RNN_SIZE

    if test:
        ISTEST = True
        EPOCHS = 3
        RNN_SIZE = 8

    if os.path.isdir ("./temp"):
        shutil.rmtree ("./temp")

    verr = train (test)
    terr = ftest (test)

    print ("___\n")
    print ("* Validation set error: {:.5f}".format (verr))
    print ("* Test set error: {:.5f}".format (terr))


if __name__ == "__main__":
    import sys
    main ("--test" in sys.argv)


