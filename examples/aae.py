# another implementation of https://github.com/hwalsuklee/tensorflow-mnist-AAE

import dnn
from dnn import split, predutil, plot, priors, optimizers
from dnn.costs import gan, cosine_distance_error
from datasets.mnist import read_data_sets
from tqdm import tqdm
import tensorflow as tf
import numpy as np
from rs4 import pathtool
import gan as _gan

class Generator (_gan.Generator):
    def discriminator (self, x):
        with tf.compat.v1.variable_scope ("discriminator", reuse = tf.compat.v1.AUTO_REUSE):
            layer = self.dense (x, 128, activation = tf.nn.relu)
            layer = self.dropout (layer)
            layer = self.dense (layer, 64, activation = tf.nn.relu)
            layer = self.dropout (layer)
            logit = self.dense (layer, 1, activation = tf.sigmoid)
        return logit

    def encode (self, x):
        with tf.compat.v1.variable_scope ("encoder", reuse = tf.compat.v1.AUTO_REUSE):
            layer = self.dense (x, 512, activation = tf.nn.elu)
            layer = self.dropout (layer)
            layer = self.dense (layer, 512, activation = tf.nn.tanh)
            layer = self.dropout (layer)
            logit = self.dense (layer, self.dim_z)
        return logit

    def make_place_holders (self):
        self.x = tf.compat.v1.placeholder ("float", [None, 784])
        self.y = tf.compat.v1.placeholder ("float", [None, self.dim_z])
        self.x_id = tf.compat.v1.placeholder(tf.float32, shape=[None, 10], name='input_img_label')
        self.z_id = tf.compat.v1.placeholder(tf.float32, shape=[None, 10], name='prior_sample_label')

    def make_optimizer (self):
        return [
            self.optimizer ("adam", self.scoped_cost (self.R_loss, ["encoder", "generator"])),
            self.optimizer ("adam", self.scoped_cost (self.D_loss, "discriminator"), self.learning_rate / 5),
            self.optimizer ("adam", self.scoped_cost (self.G_loss, "encoder")),
            self.optimizer ("adam", self.scoped_cost (self.G_loss, "encoder")) # double for speeding
        ]

    def make_logit (self):
        return self.generator (self.y)

    def make_cost (self):
        z_real = tf.concat ([self.y, self.z_id], 1)
        self.z = self.encode (self.x)
        z_fake = tf.concat ([self.z, self.x_id], 1)

        self.D_loss, self.G_loss = gan.gan_loss (self.discriminator, z_fake, z_real)

        self.cp1 = self.generator (self.z)
        # reconstuct loss
        self.R_loss = tf.reduce_mean (input_tensor=tf.math.squared_difference (self.x, self.cp1))

        self.log ("R-loss", self.R_loss)
        self.log ("G-loss", self.G_loss)
        self.log ("D-loss", self.D_loss)
        return self.R_loss + self.D_loss + self.G_loss


def main (test = False):
    mnist = read_data_sets ("data", one_hot=True, test = test)
    net = Generator ()
    net.set_tensorboard_dir ("./temp/tflog")
    net.set_learning_rate (1e-3, 0.99, 100)

    plotter = plot.Plot_Reproduce_Performance("./temp/results", 8, 8, 28, 28, 1.0)
    plotter.reset ()
    mfplotter = plot.Plot_Manifold_Learning_Result("./temp/results")

    valid_xs = mnist.test.images [0:64, :]
    valid_ys = mnist.test.labels [0:64, :]
    plotter.save_images(valid_xs.reshape  (64, 28, 28), name='input.jpg')
    valid_xs_noised = plotter.add_noise (valid_xs)
    plotter.save_images(valid_xs_noised.reshape  (64, 28, 28), name='input-noised.jpg')
    train_minibatches = split.minibatch (mnist.train.images, mnist.train.labels, 128)

    x_PMLR = mnist.test.images [0:5000, :]
    id_PMLR = mnist.test.labels [0:5000, :]

    training_epochs = test and 3 or 200
    batch_size = 128

    for epoch in range (1, training_epochs + 1):
        print ("\n* Epoch progress: {} / {} ({:.0f}%)".format (epoch, training_epochs, epoch / training_epochs * 100))
        net.set_epoch (epoch)
        total_batch = int (mnist.train.num_examples/batch_size)
        for i in range(total_batch):
            reals, labels = next (train_minibatches)
            z_id_ = np.random.randint(0, 10, size=[batch_size])
            z_id_one_hot_vector = np.zeros((batch_size, 10))
            z_id_one_hot_vector[np.arange(batch_size), z_id_] = 1
            batch_zs = priors.gaussian_mixture(reals.shape [0], net.dim_z, label_indices = z_id_)
            net.batch (reals, batch_zs, dropout_rate = 0.1, x_id = labels, z_id = z_id_one_hot_vector)

        r = net.valid (valid_xs, batch_zs [:len (valid_xs)], ops = [net.cp1], x_id = valid_ys, z_id = z_id_one_hot_vector [:len (valid_xs)]).log ()
        plotter.save_images(r.ops [0].reshape  (64, 28, 28), name = "/PRR_epoch_%02d" %(epoch) + ".jpg")
        images = net.run_for_eval (net.logit, y = mfplotter.z) [0]
        mfplotter.save_images (images.reshape (-1, 28, 28), name = "/PMRR_epoch_%02d" %(epoch) + ".jpg")
        z_PMLR = net.run_for_eval (net.z, x = x_PMLR) [0]
        mfplotter.save_scattered_image(z_PMLR, id_PMLR, name="/PMLR_map_epoch_%02d" % (epoch) + ".jpg")


if __name__ == "__main__":
    import sys

    main ("--test" in sys.argv)

