# # another implementation of https://github.com/hwalsuklee/tensorflow-mnist-VAE

import dnn
from dnn import split, predutil, plot, priors
from dnn.costs import gan, cosine_distance_error
from datasets.mnist import read_data_sets
from tqdm import tqdm
import tensorflow as tf
import numpy as np
from rs4 import pathtool

class MNI (dnn.DNN):
    dim_z = 2

    def make_place_holders (self):
        self.x = tf.compat.v1.placeholder ("float", [None, 784])
        self.y = tf.compat.v1.placeholder ("float", [None, self.dim_z])

    def make_optimizer (self):
        return self.optimizer ("adam")

    def make_cost (self):
        self.z, mu, sigma = self.encode (self.x)
        self.cp1 = self.decode (self.z)
        loss, neg_marginal_likelihood, KL_divergence = gan.vae_loss (self.cp1, self.x, mu, sigma)
        self.log ("marginal-likelihood", neg_marginal_likelihood)
        self.log ("KL-divergence", KL_divergence)
        return loss

    def make_logit (self):
        return self.decode (self.y)
    #-----------------------------------------------------

    def encode (self, x):
        with tf.compat.v1.variable_scope ("gaussian_MLP_encoder", reuse = tf.compat.v1.AUTO_REUSE):
            layer = self.dense (x, 512, activation = tf.nn.elu)
            layer = self.dropout (layer)
            layer = self.dense (layer, 512, activation = tf.nn.tanh)
            layer = self.dropout (layer)
            logit, mu, sigma = self.gaussian_encode (layer, self.dim_z)
        return logit, mu, sigma

    def decode (self, x):
        with tf.compat.v1.variable_scope ("bernoulli_MLP_decoder", reuse = tf.compat.v1.AUTO_REUSE):
            layer = self.dense (x, 512, activation = tf.nn.tanh)
            layer = self.dropout (layer)
            layer = self.dense (layer, 512, activation = tf.nn.elu)
            layer = self.dropout (layer)
            output = self.bernoulli_decode (layer, 784)
        return output

def main (test = False):
    mnist = read_data_sets ("data", one_hot=True, test = test)
    net = MNI ()
    net.set_tensorboard_dir ("./temp/tflog")
    net.set_learning_rate (1e-3, 0.99, 100)

    plotter = plot.Plot_Reproduce_Performance("./temp/results", 8, 8, 28, 28, 1.0)
    plotter.reset ()
    mfplotter = plot.Plot_Manifold_Learning_Result("./temp/results")

    valid_xs = mnist.test.images [0:64, :]
    plotter.save_images(valid_xs.reshape  (64, 28, 28), name='input.jpg')
    valid_xs_noised = plotter.add_noise (valid_xs)
    plotter.save_images(valid_xs_noised.reshape  (64, 28, 28), name='input-noised.jpg')
    train_minibatches = split.minibatch (mnist.train.images, mnist.train.labels, 128)

    x_PMLR = mnist.test.images [0:5000, :]
    id_PMLR = mnist.test.labels [0:5000, :]

    training_epochs = test and 3 or 200
    batch_size = 128

    for epoch in range (1, training_epochs + 1):
        print ("\n* Epoch progress: {} / {} ({:.0f}%)".format (epoch, training_epochs, epoch / training_epochs * 100))
        net.set_epoch (epoch)
        total_batch = int (mnist.train.num_examples/batch_size)
        for i in range(total_batch):
            batch_xs, _ = next (train_minibatches)
            batch_zs = priors.gaussian (batch_xs.shape [0], net.dim_z)
            net.batch (batch_xs, batch_zs, dropout_rate = 0.1)

        r = net.valid (valid_xs, priors.gaussian (batch_zs.shape [0], net.dim_z), [net.cp1]).log ()
        plotter.save_images(r.ops [0].reshape  (64, 28, 28), name = "/PRR_epoch_%02d" %(epoch) + ".jpg")
        images = net.run_for_eval (net.logit, y = mfplotter.z) [0]
        mfplotter.save_images (images.reshape (-1, 28, 28), name = "/PMRR_epoch_%02d" %(epoch) + ".jpg")
        z_PMLR = net.run_for_eval (net.z, x = x_PMLR) [0]
        mfplotter.save_scattered_image(z_PMLR, id_PMLR, name="/PMLR_map_epoch_%02d" % (epoch) + ".jpg")

if __name__ == "__main__":
    import sys

    main ("--test" in sys.argv)

