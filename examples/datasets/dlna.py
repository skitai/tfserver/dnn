import pickle
from dnn import datautil
import numpy as np
from dnn.datautil import pad_sequences
import os

MAX_TERMS = 32
ID_PAD = 0
ID_EOS = 1
FILE = os.path.join (os.path.dirname (__file__), "data", "dlna.pkl")
TEST_SIZE = 10000

def eos (xs):
    return np.where (xs == ID_EOS) [1]

def pad (xs):    
    return pad_sequences (xs, maxlen = MAX_TERMS, value = ID_PAD, truncating = 'pre')

def get_trainset (valid_size = 0, test = False):
    with open (FILE, "rb") as f:            
        X, Y = pickle.load (f)
    if test:
        X = X [:300]    
        Y = Y [:300]
        X, Y = pad (X), Y
        valid_size = 100
    else:    
        X, Y = pad (X [:-TEST_SIZE]), Y [:-TEST_SIZE]
    if not valid_size:
        return  X, Y
    return datautil.split (X, Y, valid_size)

def get_testset (test = False):
    with open (FILE, "rb") as f:     
        X, Y = pickle.load (f)
    test_size = TEST_SIZE
    if test:
        test_size = 100    
    return pad (X [-test_size:]), Y [-test_size:]
