from PIL import Image, ImageEnhance
import random
import cv2

def darken (img):
    img = img.copy ()
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    pimg = Image.fromarray(img)
    color_dim_factor = 0.3 * random.random() + 0.7
    contrast_dim_factor = 0.3 * random.random() + 0.7
    pimg = ImageEnhance.Color(pimg).enhance(color_dim_factor)
    pimg = ImageEnhance.Contrast(pimg).enhance(contrast_dim_factor)
    img = (img.clip(0, 255)).astype("uint8")
    low_img = img.astype('double') / 255.0

    beta = 0.5 * random.random() + 0.5
    alpha = 0.1 * random.random() + 0.9
    gamma = 3.5 * random.random() + 1.5
    low_img = beta * np.power(alpha * low_img, gamma)
    low_img = low_img * 255.0
    low_img = (low_img.clip(0, 255)).astype("uint8")
    low_img = cv2.cvtColor(low_img, cv2.COLOR_RGB2BGR)
    return low_img

